import {
    createUserListApi
} from '../api/createUser-api'

export const POST_CREATE_USER_INIT = 'POST_CREATE_USER_INIT';
export const POST_CREATE_USER_SUCCESS = 'POST_CREATE_USER_SUCCESS';
export const POST_CREATE_USER_FAILED = 'POST_CREATE_USER_FAILED';

export function createUserList(body){
    return function (dispatch){
        dispatch({ type: POST_CREATE_USER_INIT });
        createUserListApi(body)
        .then((response) => {
            console.log(response)
            return dispatch({ type: POST_CREATE_USER_SUCCESS, response })
        })
        .catch(error => dispatch({ type: POST_CREATE_USER_FAILED, error }))
    }
}