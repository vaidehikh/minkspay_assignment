import { 
    getSingleUserListApi
  } from '../api/singleUser-api';

export const GET_SINGLE_USER_LIST_INIT = 'GET_SINGLE_USER_LIST_INIT';
export const GET_SINGLE_USER_LIST_SUCCESS = 'GET_SINGLE_USER_LIST_SUCCESS';
export const GET_SINGLE_USER_LIST_FAILED = 'GET_SINGLE_USER_LIST_FAILED';

const CLEAR_LOGIN = 'CLEAR_LOGIN';

export function getSingleUserList(itemId) {
    return function (dispatch) {
      dispatch({ type: GET_SINGLE_USER_LIST_INIT });
      
      getSingleUserListApi(itemId)
      
      .then((response) => {
      
            return dispatch({ type: GET_SINGLE_USER_LIST_SUCCESS, response })
         
        })
        
        .catch(error => dispatch({ type: GET_SINGLE_USER_LIST_FAILED, error }));
    };
}

