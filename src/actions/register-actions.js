import{
    registerListApi
}from '../api/register-api'

export const POST_REGISTER_INIT = 'POST_REGISTER_INIT';
export const POST_REGISTER_SUCCESS = 'POST_REGISTER_SUCCESS';
export const POST_REGISTER_FAILED = 'POST_REGISTER_FAILED';

export function setRegister(body){
    return function (dispatch){
        dispatch({ type: POST_REGISTER_INIT });
        registerListApi(body)
        .then((response) => {
            return dispatch({ type: POST_REGISTER_SUCCESS, response })
        })
        .catch(error => dispatch({ type: POST_REGISTER_FAILED, error }))
    }
}