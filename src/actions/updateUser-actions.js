import {
    updateUserListApi
} from '../api/updateUser-api';

export const UPDATE_USER_LIST_INIT = 'UPDATE_USER_LIST_INIT';
export const UPDATE_USER_LIST_SUCCESS = 'UPDATE_USER_LIST_SUCCESS';
export const UPDATE_USER_LIST_FAILED = 'UPDATE_USER_LIST_FAILED';

export function updateUserList(itemId, name, job){
    return function (dispatch){
        dispatch({ type: UPDATE_USER_LIST_INIT });
        updateUserListApi(itemId, name, job)
        .then((response) => {
            return dispatch({ type: UPDATE_USER_LIST_SUCCESS, response })
        }) 
        .catch(error => dispatch({ type: UPDATE_USER_LIST_FAILED, error }))
    }
}
