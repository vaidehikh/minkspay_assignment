import { 
    loginAPI
  } from '../api/login-api';

export const POST_LOGIN_INIT = 'POST_LOGIN_INIT';
export const POST_LOGIN_SUCCESS = 'POST_LOGIN_SUCCESS';
export const POST_LOGIN_FAILED = 'POST_LOGIN_FAILED';

export function getLogin(body) {
  console.log(body)
  return function (dispatch) {
      dispatch({ type: POST_LOGIN_INIT });
      loginAPI(body)
        .then((response) => {
          console.log(response)
            if(response === 'failed') {
              console.log(response)
              return dispatch({ type: POST_LOGIN_FAILED, error: 'authentication failed' });
            }else {
              return  dispatch({ type: POST_LOGIN_SUCCESS, response })
             }
        })
        
        .catch((error) => dispatch({ type: POST_LOGIN_FAILED, error }));
    };
}


