import { 
    getUserListApi
  } from '../api/user-api';

export const GET_USER_LIST_INIT = 'GET_USER_LIST_INIT';
export const GET_USER_LIST_SUCCESS = 'GET_USER_LIST_SUCCESS';
export const GET_USER_LIST_FAILED = 'GET_USER_LIST_FAILED';

const CLEAR_LOGIN = 'CLEAR_LOGIN';

export function getUserList() {
    return function (dispatch) {
      dispatch({ type: GET_USER_LIST_INIT });
      
      getUserListApi()
      
      .then((response) => {
      
            return dispatch({ type: GET_USER_LIST_SUCCESS, response })
         
        })
        
        .catch(error => dispatch({ type: GET_USER_LIST_FAILED, error }));
    };
}

