
export function loginAPI(body) {
  // const header = `Bearer ${sessionStorage.getItem('access_token')}`;
  var myHeaders = new Headers();
  myHeaders.append("Content-Type", 'application/json',);

  var raw = JSON.stringify({ "email": "eve.holt@reqres.in", "password": "cityslicka" });

  var requestOptions = {
    method: 'POST',
    mode: "cors",
    headers: myHeaders,
    body: JSON.stringify(body),
    redirect: 'follow'
  };


  return fetch(`https://reqres.in/api/login`, requestOptions)

    .then((res) => {
      console.log(res)
      if (res.status === 401) {
        return 'invalid authentication';
      } else if (res.status !== 200) {
        return 'failed';
      }
      return res.json();
    }).catch(err => 'failed');
}