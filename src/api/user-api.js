export function getUserListApi() {

    console.log("getUserListApi method call")
        ;
    var requestOptions = {
        method: 'GET',
        redirect: 'follow',

    };

    return fetch(`https://reqres.in/api/users?page=1`, requestOptions)

        // .then(response => response.json())
        // .then(result => console.log(result))
        // .catch(error => console.log('error', error));
        .then((res) => {
            if (res.status === 401) {
                return 'invalid authentication';
            } else if (res.status !== 200) {
                return 'failed';
            }
            return res.json();
        })
        // .then((result) =>  {Object.values(result)} )
        .catch(err => err);
}