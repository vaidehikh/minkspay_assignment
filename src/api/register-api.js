export function registerListApi(body){
    var myHeaders = new Headers();
    myHeaders.append(
    "Content-Type", "application/json");

    var requestOptions = {
        method: 'POST',
        //mode: "cors",
        headers: myHeaders,
        //body: raw,
        body: JSON.stringify(body),
        redirect: 'follow'
        };
    return fetch(`https://reqres.in/api/register`, requestOptions)
    .then((res) => {
        return res.json()
    })
    .catch(err => err);
}