export function updateUserListApi(itemId, name, job){
    console.log("updateUserListApi method call " + itemId + name + job);
   
    var requestOptions = {
        method: 'PUT',
         redirect: 'follow',
         headers: {
            "Content-Type": "application/json",
           
        },
        body: JSON.stringify({name, job})
    };

    return fetch(`https://reqres.in/api/users/${itemId}`, requestOptions)

        .then((res) => {
            if (res.status === 401) {
                return 'invalid authentication';
            } else if (res.status !== 200) {
                return 'failed';
            }
           
            return res.json();
        })
       
        .catch(err => err);
}

