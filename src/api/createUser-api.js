export function createUserListApi(body){
    console.log(body)
    var myHeaders = new Headers();
    myHeaders.append("Access-Control-Allow-Origin",
        "Content-Type", "application/json");

    var raw = JSON.stringify({"name":"morpheus","job":"leader"});

    var requestOptions = {
    method: 'POST',
    mode: "cors",
    headers: myHeaders,
    //body: raw,
    body: JSON.stringify(body),
    redirect: 'follow'
    };

    return fetch(`https://reqres.in/api/users`, requestOptions)
    .then((res) => {
        // if (res.status === 401) {
        //     return 'invalid authentication';
        // } else if (res.status !== 200) {
        //     console.log("in else if")
        //     return 'failed';
        // }
       
        return res.json();
    })
   
    .catch(err => err);
}