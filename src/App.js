import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Switch, Route, Redirect, withRouter } from 'react-router-dom';
import Home from './components/Home';
import { createBrowserHistory } from 'history';
// import { Provider } from 'react-redux';
// import store from './configureStore';

function App() {
  return (
    <div className="App">
      {/* <Provider store={store}> */}
     <BrowserRouter history={createBrowserHistory}>
     {/* <div>
       <Home/>
     </div> */}
     <Home/>
     {/* <Route path="" component={Home}/> */}
     </BrowserRouter >
     {/* </Provider> */}
    </div>
  );
}

export default App;
