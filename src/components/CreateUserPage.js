import React, { Component } from 'react';
import { FormGroup, Form, Label, Input, Button } from 'reactstrap';
import { createUserList } from '../actions/Actions';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
class CreateUserPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            job : '',
        }
       this.handleOnChange = this.handleOnChange.bind(this)
       this.handleOnClick = this.handleOnClick.bind(this)
    }
    handleOnChange(event){
        this.setState({
            [event.target.id] : event.target.value,
        });
    }
    // componentDidMount(){
    //     var myHeaders = new Headers();
    //         myHeaders.append("Access-Control-Allow-Origin",
    //             "Content-Type", "application/json");

    //         var raw = JSON.stringify({"name":"morpheus","job":"leader"});

    //         var requestOptions = {
    //         method: 'POST',
    //         headers: myHeaders,
    //         body: raw,
    //         redirect: 'follow'
    //         };

    //         fetch("https://reqres.in/api/users", requestOptions)
    //         .then(response => response.text())
    //         .then((result) => console.log(result))
    //         .catch(error => console.log('error', error));
    //      }
    handleOnClick(){
        const { name, job } = this.state
        this.props.dispatch(createUserList({name, job}))
    }

         render(){
             const { name, job } = this.state
             return(
                 <div>
                     <Form>
                        <FormGroup>
                            <Label htmlFor='name'>Name</Label>
                            <Input type="text" id="name" name="name" value={name}
                            onChange={this.handleOnChange}/>
                             <Label htmlFor='job'>Job</Label>
                             <Input type="text" id="job" name="job" value={job}
                             onChange={this.handleOnChange}/>
                             <Button onClick={this.handleOnClick}>Create</Button>
                        </FormGroup>
                     </Form>
                 </div>
             )
         }
    }

    const select = (state) => ({
        createUserList: state.createUser.createUserList,
        
      });
      
   export default withRouter(connect(select)(CreateUserPage));
    //export default CreateUserPage;