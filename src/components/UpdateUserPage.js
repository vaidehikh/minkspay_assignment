import React, { Component } from 'react';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { updateUserList } from '../actions/Actions';

class UpdateUserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            job: '',
            items: [],
        }

        this.onChange = this.onChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }
    onChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });

    }
    handleSubmit() {
        const { name, job } = this.state;
        this.props.dispatch(updateUserList(this.props.match.params.itemId, name, job))
        console.log(name)
    }


    render() {
        const { name, job } = this.state;
        return (
            console.log(this.props.updatedUser.name),
            <div>
                <Form className='content'>
                    <FormGroup>
                        <Label htmlFor='name'>Name</Label>
                        <Input type="text" id="name" name="name" value={name}
                            onChange={this.onChange} />
                        <Label htmlFor='name'>Job</Label>
                        <Input type="text" id="job" name="job" value={job}
                            onChange={this.onChange} />
                        <Button color="primary"

                            onClick={this.handleSubmit}>Submit</Button>
                    </FormGroup>
                </Form>
            </div>
        )
    }
}

const select = (state) => ({
    updatedUser: state.updateuser.updatedUser,

});
export default (connect(select)(UpdateUserPage));
