import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSingleUserList } from '../actions/Actions';
import { withRouter } from 'react-router-dom';
import '../App.css';

class SingleUserPage extends Component {
    constructor(props){
        super(props);
            this.state = {
                singleUser : [],

            };
    
    }
  
    render(){
        
        return(
            console.log(this.props.singleUserList.data),
            <div>
                <h1>hi</h1>
                {this.props.singleUserList.data && Object.keys(this.props.singleUserList.data).map((item, i) => (
                   console.log(item),
                   item !== 'avatar' ?
                   <div className="card" key={i}> 
                   <div className="card-body">
                  <h5 className='card-title'  >
                    { this.props.singleUserList.data[item] }
                  </h5>
                  
                 </div>
                 </div>
                : <div>
                <img src={ this.props.singleUserList.data[item] } style={{width: '180px', height: '180px', alignSelf: 'center'}}  
                                   className="rounded-circle img-fluid"
                                   alt="contentItem.profilepicture"/>
                                   </div>
                     ))}


                  
            </div>
        )
    }
                     
}

const select = (state) => ({
    singleUserList: state.singleuser.singleUserList,
    
  });
  export default withRouter(connect(select)(SingleUserPage));
  

