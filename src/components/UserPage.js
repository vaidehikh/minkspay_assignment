import React, { Component } from "react";
import { getUserList, getSingleUserList, updateUserList  } from "../actions/Actions";
import { connect } from 'react-redux';
import { HashRouter as Router, Route, Redirect, Link } from 'react-router-dom';
import { withRouter, Switch, browserHistory } from "react-router-dom";
import SingleUserPage from "./SingleUserPage";


class UserPage extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this)
    this.handleUpdate = this.handleUpdate.bind(this)
    this.handleCreate = this.handleCreate.bind(this)
  }
  handleClick(itemId){
    console.log(itemId)
   
     this.props.dispatch(getSingleUserList(itemId))
    
    this.props.history.push(`/singleuserpage/${itemId}`);
    
  }
  handleUpdate(itemId){
    this.props.dispatch(updateUserList(itemId))
    this.props.history.push(`/updateuserpage/${itemId}`)
  }
  handleCreate(){
   // this.props.dispatch(updateUserList(itemId))
    this.props.history.push(`/createuserpage`)
  }
     componentDidMount(props) {
     
      this.props.dispatch(getUserList());
        
       }
       
    render() {
     
        return(
          console.log(this.props.userList.data),
            <div>
              
             <button onClick={this.handleCreate}>Create</button>
             {
             this.props.userList.data && Object.values(this.props.userList.data).map((item, i) => (
             console.log(item),
              <div className="card" key={i}> 
              <div className="card-body" >
             <h5 className='card-title' value={item.id} onClick={() => this.handleClick(item.id)} >
               { item.id }
             </h5>
             <h6 className='card-subtitle'>{item.first_name}{item.last_name}</h6>
             
             <h6 className='card-subtitle'>{item.email}</h6>
             <br></br>
             <button onClick={() => this.handleUpdate(item.id)}>Update</button>
             
            </div>

            </div>
                         
           ))
              
          }
            </div>
         )
        }
    }
    
    const select = (state) => ({
        userList: state.user.userList,
        
      });
      
      export default withRouter(connect(select)(UserPage));

    