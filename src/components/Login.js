import React, { Component } from "react";
import { Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalHeader, Breadcrumb, BreadcrumbItem, NavLink, Row, Col, Nav, NavItem } from 'reactstrap';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { getLogin, setRegister } from '../actions/Actions';
import { connect } from 'react-redux';
const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);
const isNumber = (val) => isNaN(Number(val));
const vaildEmail = (val) => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val)

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isAuthFailed: false,
            isModalOpen: false,
            isRoute: false,
            // registerEmail: '',
            // registerPassword: ''
        };
        this.toggleModal = this.toggleModal.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onRegisterClick = this.onRegisterClick.bind(this);
        this.onRegisterChange = this.onRegisterChange.bind(this);
    }

    componentWillReceiveProps(newProps) {
        console.log(newProps.authStatus)
        // if(newProps.authStatus == 'success'){
        //     this.setState({
        //         isRoute : false
        //     });
        // }
        // else{
        //     this.setState({
        //         isRoute : true
        //     });
        // }
        if (this.props.authStatus === 'started' && newProps.authStatus === 'failed') {
            this.setState({
                isAuthFailed: true
            });
            console.log('in if')
        }
        else {
            
            this.setState({
                isAuthFailed: false,
                //isRoute : true,
                email: '',
                password: ''
            });
           // this.props.history.push(`/user`);
        //    console.log(this.props.authStatus)
        if(newProps.authenticated === true && newProps.authStatus === 'success'){
        console.log(newProps.authenticated)
        this.props.history.push(`/user`);
        }
        }
       
    }

    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen
        })
    }
    onChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });

    }
    onRegisterChange(event) {
        this.setState({
            [event.target.id]: event.target.value
        });

    }


    isFormInValid() {
        let isInvalid = false;
        if (this.state.email === undefined || this.state.email === '' || !vaildEmail(this.state.email)) {
            isInvalid = true;
        }
        if (this.state.password === undefined || this.state.password === '') {
            isInvalid = true;
        }
        return isInvalid;
    }
    onSubmit() {
        const { email, password } = this.state;
        console.log(this.props.authenticated)
        this.props.dispatch(getLogin({
            email,
            password
        }));
       
    }
    onRegisterClick(){
        // const { registerEmail, registerPassword } = this.state;
        const { email, password } = this.state;
        this.props.dispatch(setRegister({
            // registerEmail,
            // registerPassword
            email,
            password
        }))
    }
    render() {
        // const { registerEmail, registerPassword } = this.state;
        const { email, password } = this.state;
        return (
            <>

                <Form className='content'>
                    <FormGroup>
                        <Label htmlFor='email'>email</Label>
                        <Input type="text" id="email" name="email"
                           
                            value={email}
                            onChange={this.onChange} />
                    </FormGroup>
                    <FormGroup>
                        <Label htmlFor="password">Password</Label>
                        <Input type="password" id="password" name="password"
                           
                            value={password}
                            onChange={this.onChange} />
                    </FormGroup>


                    <FormGroup >
                        <Button color="primary"
                            disabled={this.isFormInValid()}
                            onClick={this.onSubmit}>Login</Button>

                    </FormGroup>
                    <FormGroup>
                        {this.state.isAuthFailed && <Label htmlFor='authentication'> Authentication failed</Label>}
                    </FormGroup>


                    <div className="container">
                        <div className='row'>

                            <Nav className='ml-auto' navbar>
                                <NavItem>
                                    <Button color="primary"
                                        onClick={this.toggleModal}>Register</Button>
                                </NavItem>
                            </Nav>
                        </div>
                    </div>
                </Form>
                <div>
                    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal}>Register</ModalHeader>
                        <ModalBody>
                            <LocalForm >
                                <Row className='form-group'>
                                    <Label htmlFor='firstname' md={2}>First Name</Label>
                                    <Col md={10}>
                                        <Control.text model='.firstname' id='firstname' name='firstname'
                                            placeholder='First Name' className='form-control'
                                            validators={{
                                                required, minLength: minLength(3), maxLength: maxLength(15)
                                            }}
                                        />
                                        <Errors
                                            className='text-danger'
                                            model='.firstname'
                                            show='touched'
                                            messages={{
                                                required: 'Required',
                                                minLength: 'Must be greater then 2 characters',
                                                maxLength: 'Must be 15 character or less'
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row className='form-group'>
                                    <Label htmlFor='lastname' md={2}>Last Name</Label>
                                    <Col md={10}>
                                        <Control.text model='.lastname' id='lastname' name='lastname'
                                            placeholder='Last Name' className='form-control'
                                            validators={{
                                                required, minLength: minLength(3), maxLength: maxLength(15)
                                            }}
                                        />

                                        <Errors
                                            className='text-danger'
                                            model='.lastname'
                                            show='touched'
                                            messages={{
                                                required: 'Required',
                                                minLength: 'Must be greater then 2 characters',
                                                maxLength: 'Must be 15 character or less'
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row className='form-group'>
                                    <Label htmlFor='telnum' md={2}>Contact Tel.</Label>
                                    <Col md={10}>
                                        <Control.text model='.tel' id='telnum' name='telnum'
                                            placeholder='Telephone Number' className='from-control'
                                            validators={{
                                                required, minLength: minLength(3),
                                                maxLength: maxLength(15), isNumber
                                            }}
                                        />
                                        <Errors
                                            className='text-danger'
                                            model='.telnum'
                                            show='touched'
                                            messages={{
                                                required: 'Required',
                                                minLength: 'Must be greater then 2 characters',
                                                maxLength: 'Must be 15 character or less',
                                                isNumber: 'Must be a number'
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row className='form-group'>
                                    <Label htmlFor='email' md={2}>Email</Label>
                                    <Col md={10}>
                                        <Control.text model='.email' id='email' name='email'
                                            placeholder='Email' className='form-control'
                                            value={email}
                                            validators={{
                                                required, vaildEmail
                                            }}
                                            onChange={this.onRegisterChange}
                                        />
                                        <Errors
                                            className='text-danger'
                                            model='.email'
                                            show='touched'
                                            messages={{
                                                required: 'Required',
                                                vaildEmail: 'Invalid Email Address'
                                            }}
                                        />
                                    </Col>
                                </Row>
                                <Row className='form-group'>
                                    <Label htmlFor='password' md={2}>Password</Label>
                                    <Col md={10}>
                                        <Control.password model='.password' id='password' name='password'
                                            placeholder='Password' className='form-control'
                                            value={password}

                                             onChange={this.onRegisterChange}
                                        />
                                         {/* <Errors
                                            className='text-danger'
                                            model='.password'
                                            show='touched'
                                            messages={{
                                                required: 'Required',
                                                vaildEmail: 'Invalid password'
                                            }}
                                        /> */}
                                    </Col>
                                </Row>
                                <Row className='form-group'>
                                    <Col md={{ size: 10, offset: 2 }}>
                                        <Button type='submit' onClick={this.onRegisterClick} color='primary'>
                                            Submit
                                    </Button>
                                    </Col>
                                </Row>
                            </LocalForm>
                        </ModalBody>
                    </Modal>
                </div>

            </>
        )
    }
}
const select = (state) => ({
    authStatus: state.login.authStatus,
    authenticated: state.login.authenticated,
    // registerList: state.register.registerList,
});
export default (connect(select)(Login));
