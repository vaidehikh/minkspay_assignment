import React, { Component } from "react";
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';
import UserPage from "./UserPage";
import Login from "./Login";
import { connect } from 'react-redux';
import SingleUserPage from "./SingleUserPage";
import UpdateUserPage from "./UpdateUserPage";
import CreateUserPage from "./CreateUserPage";

class Home extends Component {
  
    constructor(props){
        super(props);
       
    }

    render() {
        
        return(
            
            <div>
                
               <div className="container">
                  
                      <Breadcrumb>
                      <BreadcrumbItem>
                                <Link to = '/'>Welcome</Link>
                            </BreadcrumbItem>
                          
                        </Breadcrumb>
                    </div> 
                    
                    <Switch>
                   
                        <Route path='/' exact component={Login}/>
                        <Route  path='/user' exact component={() => <UserPage/>}/>
                        <Route  path='/singleuserpage/:itemId' exact component={SingleUserPage}/>
                        <Route  path='/updateuserpage/:itemId' exact component={UpdateUserPage}/>
                        <Route path='/createuserpage' component={CreateUserPage}/>
                      
                    </Switch>
                    
            </div>
             
       
        )
    }
}

export default withRouter(Home);