import{
    POST_REGISTER_INIT,
    POST_REGISTER_SUCCESS,
    POST_REGISTER_FAILED
} from '../actions/Actions';

export const initialState = {
    registerList: []
};
 export default function registerReducer(state=initialState, action){
     const handlers = {
         [POST_REGISTER_INIT]: (state) => ({
             ...state
         }),
         [POST_REGISTER_SUCCESS]: (state, action) => ({
             ...state,
             registerList: action.response
         }),
         [POST_REGISTER_FAILED]: (state) => ({
             ...state,
             registerList: [],
         })
     };
     const handler = handlers[action.type];
     if(!handler) return state;
     console.log(handler(state, action))
     return {...state, ...handler(state, action)};
 }