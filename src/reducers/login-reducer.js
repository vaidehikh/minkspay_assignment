import {
  POST_LOGIN_INIT,
  POST_LOGIN_SUCCESS,
  POST_LOGIN_FAILED,

} from '../actions/Actions';

export const initialState = {
  authenticated: false,
  authStatus: undefined,
  token: undefined,

};

export default function loginReducer(state = initialState, action) {
  console.log(action)
  const handlers = {
    [POST_LOGIN_INIT]: (state) => ({
      ...state,
      authStatus: 'started'
    }),
    [POST_LOGIN_SUCCESS]: (state, action) => ({
      ...state,
      authenticated: true,
      token: action.response,

      authStatus: 'success'

    }),
    [POST_LOGIN_FAILED]: (state) => ({
      ...state,
      authenticated: false,
      token: undefined,
      authStatus: 'failed'
    }),

  };


  const handler = handlers[action.type];
  if (!handler) return state;
  console.log(handler(state, action))
  return { ...state, ...handler(state, action) };
}

