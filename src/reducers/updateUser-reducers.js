import {
    UPDATE_USER_LIST_INIT,
    UPDATE_USER_LIST_SUCCESS,
    UPDATE_USER_LIST_FAILED 
 } from '../actions/Actions';
 
 export const initialState = {
    
  updatedUser : []
};

export default function updateUserReducer(state = initialState, action) {
const handlers = {
  [UPDATE_USER_LIST_INIT]: (state) => ({
    ...state,
  }),
  [UPDATE_USER_LIST_SUCCESS]: (state, action) => ({
    ...state,
    updatedUser: action.response,
    
  }),
  [UPDATE_USER_LIST_FAILED]: (state) => ({
    ...state,
    updatedUser: [],
  }),
  
};

    const handler = handlers[action.type];
    if (!handler) return state;
    console.log(state)
    console.log(handler(state, action))
    return { ...state, ...handler(state, action) };
  }
  
  