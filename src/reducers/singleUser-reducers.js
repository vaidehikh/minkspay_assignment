import {
    GET_SINGLE_USER_LIST_INIT,
    GET_SINGLE_USER_LIST_SUCCESS,
    GET_SINGLE_USER_LIST_FAILED 
 } from '../actions/Actions';

 export const initialState = {
    
    singleUserList : []
};


const handlers = {
  [GET_SINGLE_USER_LIST_INIT]: (state) => ({
    ...state,
    
  }),
  [GET_SINGLE_USER_LIST_SUCCESS]: (state, action) => ({
    ...state,
    singleUserList: action.response,
    
  }),
  [GET_SINGLE_USER_LIST_FAILED]: (state) => ({
    ...state,
    singleUserList: [],
  }),

};
export default function singleUserReducer(state = initialState, action) {
  
    const handler = handlers[action.type];
    if (!handler) return state;
    console.log(handler(state, action))
    return { ...state, ...handler(state, action) };
  }
  
  