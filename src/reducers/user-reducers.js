import {
    GET_USER_LIST_INIT,
    GET_USER_LIST_SUCCESS,
    GET_USER_LIST_FAILED 
 } from '../actions/Actions';

 export const initialState = {
    
    userList : []
};

export default function userReducer(state = initialState, action) {
const handlers = {
  [GET_USER_LIST_INIT]: (state) => ({
    ...state,
  }),
  [GET_USER_LIST_SUCCESS]: (state, action) => ({
    ...state,
    userList: action.response,
    
  }),
  [GET_USER_LIST_FAILED]: (state) => ({
    ...state,
    userList: [],
  }),
  
};

    const handler = handlers[action.type];
    if (!handler) return state;
    
    return { ...state, ...handler(state, action) };
  }
  
  