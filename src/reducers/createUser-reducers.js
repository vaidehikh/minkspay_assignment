import {
    POST_CREATE_USER_INIT,
    POST_CREATE_USER_SUCCESS,
    POST_CREATE_USER_FAILED
} from '../actions/Actions';

export const initialState = {
    createUserList: []
};

export default function createUserReducer(state = initialState, action){
    const handlers = {
        [POST_CREATE_USER_INIT]: (state) => ({
            ...state,
        }),
        [POST_CREATE_USER_SUCCESS]: (state, action) => ({
            ...state,
            createUserList: action.response,
        }),
        [POST_CREATE_USER_FAILED]: (state) => ({
            ...state,
            createUserList: [],
        })
    };
    const handler = handlers[action.type];
    if(!handler) return state;
    console.log(handler(state, action))
    return {...state, ...handler(state, action)};
}