import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import user from './reducers/user-reducers';
import singleuser from './reducers/singleUser-reducers';
import login from './reducers/login-reducer';
import register from './reducers/register-reducer';
import updateuser from './reducers/updateUser-reducers'
import createUser from './reducers/createUser-reducers'

const reducers = combineReducers({
    user,
    singleuser,
    login,
    updateuser,
    createUser,
    register,
  });
  const initialState = {};
  const enhancers = [];
  const middleware = [thunk];

  const { devToolsExtension } = window;
    if (typeof devToolsExtension === 'function') {
      enhancers.push(devToolsExtension());
    } 
  //}
  
  
  const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);
  
  const store = createStore(reducers, initialState, composedEnhancers);


  
  export default store;